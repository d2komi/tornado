using System.Data;
using System.Net;

using TORNADO_API.Models;
using TORNADO_API.Models.Repository.BankerMappingRepository;
using TORNADO_API.Utility;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TORNADO_API.Controllers;

namespace TORNADO_API.Controllers {
    [Route ("api/[controller]")]

    public class BankerMappingController : Controller {
        public IBankerMappingRepository _repository;
        public BankerMappingController (IBankerMappingRepository repo) {
            _repository = repo;
        }

        [Route("ImportMappingFromSource/{Flag}/{ClientName}/{ProjName}/{TableName}")]
        [HttpGet]
        public IActionResult ImportMappingFromSource(string Flag,string ClientName,string ProjName,string TableName)
        {
            try
            {
                var Results = _repository.ImportMappingFromSourceList(Flag,ClientName,ProjName,TableName);
                return CommonController.Instance.ReturnResponse(ResponseStatusCode.Success, "success", Results);
            }
            catch (System.Exception e)
            {
                return CommonController.Instance.ReturnResponse(ResponseStatusCode.Failed, e.Message, "null");
            }
        }
    }
}
