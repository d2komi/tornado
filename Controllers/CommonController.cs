using System;
using TORNADO_API.Utility;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Collections.Generic;
using TORNADO_API.Models;

namespace TORNADO_API.Controllers
{
#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
    public class CommonController : Controller
    {

        private static CommonController instance = null;
        public static CommonController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CommonController();
                }
                return instance;
            }
        }

        private CommonController()
        {

        }

        /// <summary>
        /// Get the HTTP Response
        /// </summary>
        /// <param name="statuscode">HTTP Status Code</param>
        /// <param name="message">To return in response as message</param>
        /// <param name="Data">Data to return in HTTP Response</param>
        /// <returns></returns>
        public IActionResult ReturnResponse(int statuscode, string message, String Data)
        {
            try
            {
                return StatusCode(statuscode,new RequestResult
                {
                    Results = new { Status = statuscode, Msg = message },
                    Msg = message,
                    Data = Data
                });
            }
            catch (Exception ex)
            {
                return StatusCode(ResponseStatusCode.Failed,new RequestResult
                {
                    Results = new { Status = RequestState.Failed, Msg = "CC_01" },
                    Msg = "Failed",
                    Data = ex.Message
                });
            }

        }

        public IActionResult ReturnResponse(int statuscode, string message, Object Data)
        {
            try
            {
                return StatusCode(statuscode, new RequestResult
                {
                    Results = new { Status = statuscode, Msg = message },
                    Msg = message,
                    Data = Data
                });
            }
            catch (Exception ex)
            {
                return StatusCode(ResponseStatusCode.Failed, new RequestResult
                {
                    Results = new { Status = RequestState.Failed, Msg = "CC_01" },
                    Msg = "Failed",
                    Data = ex.Message
                });
            }

        }

        /// <summary>
        /// Get the HTTP Response
        /// </summary>
        /// <param name="statuscode">HTTP Status Code</param>
        /// <param name="message">To return in response as message</param>
        /// <param name="Data">Data to return in HTTP Response</param>
        /// <returns></returns>
        public IActionResult ReturnResponse(int statuscode, string message, DataSet Data)
        {
            try
            {
                return StatusCode(statuscode, new RequestResult
                {
                    Results = new { Status = statuscode, Msg = message },
                    Msg = message,
                    Data = Data
                });
            }
            catch (Exception ex)
            {
                return StatusCode(ResponseStatusCode.Failed, new RequestResult
                {
                    Results = new { Status = RequestState.Failed, Msg = "CC_01" },
                    Msg = "Failed",
                    Data = ex.Message
                });
            }

        }


        // public List<LinkInfo> GetLinks(PagedList list, ProductReqModel productReq,string routeName,string version)
        // {
        //     var links = new List<LinkInfo>();

        //     //if (list.HasPreviousPage)
        //     //    links.Add(CreateLink(routeName, list.PreviousPageNumber,
        //     //               list.PageSize, productReq.CustomerId, productReq.CategoryId, productReq.SubCategoryId, "previousPage", "GET", version, productReq.TypeId));

        //     //links.Add(CreateLink(routeName, list.PageNumber,
        //     //               list.PageSize, productReq.CustomerId, productReq.CategoryId, productReq.SubCategoryId, "self", "GET"));

        //     if (list.HasNextPage)
        //     { 
        //         links.Add(CreateLink(routeName, list.NextPageNumber,
        //                    list.PageSize, productReq.CustomerId, productReq.CategoryId, productReq.SubCategoryId, "nextPage", "GET", version, productReq.TypeId));
        //     }
        //     else
        //     {
        //         links.Add(CreateLink(routeName, list.PreviousPageNumber,
        //         list.PageSize, productReq.CustomerId, productReq.CategoryId, productReq.SubCategoryId, "previousPage", "GET", version, productReq.TypeId));
        //     }

        //         return links;
        // }

        // public List<LinkInfo> GetRecipeLinks(PagedList list, RecipeReqModel recipeReq, string routeName)
        // {
        //     var links = new List<LinkInfo>();

        //     //if (list.HasPreviousPage)
        //     //    links.Add(CreateRecipeLink(routeName, list.PreviousPageNumber,
        //     //               list.PageSize, recipeReq.Ingredients, recipeReq.CustomerId, "previousPage", "GET"));
            
        //     //links.Add(CreateLink(routeName, list.PageNumber,
        //     //               list.PageSize, productReq.CustomerId, productReq.CategoryId, productReq.SubCategoryId, "self", "GET"));

        //     if (list.HasNextPage)
        //         links.Add(CreateRecipeLink(routeName, list.NextPageNumber,
        //                    list.PageSize, recipeReq.Ingredients, recipeReq.CustomerId, "nextPage", "GET"));

        //     return links;
        // }

        // public LinkInfo CreateRecipeLink(
        //     string routeName, int pageNumber, int pageSize, string Ingredients, string CustomerId, string rel, string method)
        // {
        //     return new LinkInfo
        //     {
        //         Href = routeName + "?PageNumber=" + pageNumber + "&PageSize=" + pageSize + "&Ingredients=" + Ingredients + "&CustomerId=" + CustomerId +"",
        //         //urlHelper.Link(routeName, new { PageNumber = pageNumber, PageSize = pageSize, CustomerId = customerId, SubCategoryId = subCategoryId }),// this.Url.Action(routeName, "Product", new { PageNumber = pageNumber, PageSize = pageSize, CustomerId = customerId, SubCategoryId = subCategoryId }),// Url.Link(routeName, new { PageNumber = pageNumber, PageSize = pageSize, CustomerId= customerId, SubCategoryId= subCategoryId }),
        //         Rel = rel,
        //         Method = method
        //     };
        // }


        // public LinkInfo CreateLink(
        //     string routeName, int pageNumber, int pageSize, string customerId, int CategoryId, int subCategoryId,
        //     string rel, string method, string version, int TypeId)
        // {
        //     return new LinkInfo
        //     {
        //         Href = routeName + "?PageNumber=" + pageNumber + "&PageSize=" + pageSize + "&CustomerId=" + customerId + "&CategoryId=" + CategoryId + "&SubCategoryId=" + subCategoryId + "&version="+ version + "&TypeId=" + TypeId + "",
        //         //urlHelper.Link(routeName, new { PageNumber = pageNumber, PageSize = pageSize, CustomerId = customerId, SubCategoryId = subCategoryId }),// this.Url.Action(routeName, "Product", new { PageNumber = pageNumber, PageSize = pageSize, CustomerId = customerId, SubCategoryId = subCategoryId }),// Url.Link(routeName, new { PageNumber = pageNumber, PageSize = pageSize, CustomerId= customerId, SubCategoryId= subCategoryId }),
        //         Rel = rel,
        //         Method = method
        //     };
        // }
       


    }
}