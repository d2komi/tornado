using System.Data;
using System.Net;

using TORNADO_API.Models;
using TORNADO_API.Models.Repository.ExcelErrorRepository;
using TORNADO_API.Utility;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TORNADO_API.Controllers;

namespace TORNADO_API.Controllers {
    [Route ("api/[controller]")]

    public class ExcelErrorDetailsController : Controller {
        public IExcelErrorRepository _repository;
        public ExcelErrorDetailsController (IExcelErrorRepository repo) {
            _repository = repo;
        }

        [Route("Summary")]
        [HttpGet]
        public IActionResult GetErorrFileList()
        {
            try
            {
                var Results = _repository.GetErorrFileList();
                return CommonController.Instance.ReturnResponse(ResponseStatusCode.Success, "success", Results);
            }
            catch (System.Exception e)
            {
                return CommonController.Instance.ReturnResponse(ResponseStatusCode.Failed, e.Message, "null");
            }
        }

        [Route("List/{tableName}")]
        [HttpGet]
        public IActionResult GetErorrList(string tableName)
        {
            try
            {
                var Results = _repository.GetErorrList(tableName);
                return CommonController.Instance.ReturnResponse(ResponseStatusCode.Success, "success", Results);
            }
            catch (System.Exception e)
            {
                return CommonController.Instance.ReturnResponse(ResponseStatusCode.Failed, e.Message, "null");
            }
        }
    }
}
