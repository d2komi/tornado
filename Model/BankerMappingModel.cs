using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

// using TORNADO_API.Models.Repository.ReportGenerationRepository;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TORNADO_API.Models {
   
    public class BankerMappingModel
    {
        [JsonIgnore]
        public IConfiguration _iconfiguration;
        public BankerMappingModel(IConfiguration iconfiguration) {
            _iconfiguration = iconfiguration;
        }

        public DataSet ImportMappingFromSourceList(string Flag,string ClientName,string ProjName,string TableName)
        {
            string Connection = _iconfiguration.GetValue<string>("ConnectionStrings:Connection_TORNADO");

            Database database;
            database = new SqlDatabase(Connection);
            DbCommand command = database.GetStoredProcCommand("[dbo].[MasterMappingSelect]");

            DataSet ds = new DataSet();
            using (command)
            {
                try
                {
                    database.AddInParameter(command, "@Flag", System.Data.DbType.String, Flag);
                    database.AddInParameter(command, "@ClientName", System.Data.DbType.String, ClientName);
                    database.AddInParameter(command, "@ProjName", System.Data.DbType.String, ProjName);
                    database.AddInParameter(command, "@TableName", System.Data.DbType.String, TableName);

                    ds = database.ExecuteDataSet(command);
                    return ds;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

    }
}
