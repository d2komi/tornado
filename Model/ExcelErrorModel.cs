using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

// using TORNADO_API.Models.Repository.ReportGenerationRepository;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TORNADO_API.Models {
    public class SearchModel
    {
        public string FileName { get; set; }
        public int? Pagesize { get; set; }
        public int? Pagenum { get; set; }
    }
    public class ExcelErrorModel
    {
        [JsonIgnore]
        public IConfiguration _iconfiguration;
        public ExcelErrorModel(IConfiguration iconfiguration) {
            _iconfiguration = iconfiguration;
        }

    

        public DataSet GetErorrFileList () {
            string Connection = _iconfiguration.GetValue<string> ("ConnectionStrings:Connection_TORNADO");

            Database database;
            database = new SqlDatabase (Connection);
            DbCommand command = database.GetStoredProcCommand ("ErrorDisplaySummary");

            DataSet ds = new DataSet ( );
            using (command) {
                try {
                    ds = database.ExecuteDataSet (command);
                    return ds;
                } catch (Exception ex) {
                    throw ex;
                }
            }
        }

        public DataSet GetErorrList(string tableName)
        {
            string Connection = _iconfiguration.GetValue<string>("ConnectionStrings:Connection_TORNADO");

            Database database;
            database = new SqlDatabase(Connection);
            DbCommand command = database.GetStoredProcCommand("ErrorDisplayList");

            DataSet ds = new DataSet();
            using (command)
            {
                try
                {
                    database.AddInParameter(command, "@TARGET_TABLE_NAME", System.Data.DbType.String, tableName);
                    ds = database.ExecuteDataSet(command);
                    return ds;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

    }
}
