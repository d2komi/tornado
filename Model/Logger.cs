using System;
using System.IO;

namespace TORNADO_API.Models {

    public static class Logger {

        public static void PrintLog (string Message, string Event) {
            string date = DateTime.Now.ToString ("dd") + DateTime.Now.ToString ("MMMM") + DateTime.Now.ToString ("yyyy");
            string fileName = @"C:\FORMTRAKKLOGS\" + date + "_PROD.txt";
            DirectoryInfo di = new DirectoryInfo ("C:\\FORMTRAKKLOGS\\");
            if (!di.Exists)
                di.Create ( );
            try {

                if (!File.Exists (fileName.Trim ( ))) {
                    FileStream fs1 = new FileStream (fileName, FileMode.OpenOrCreate, FileAccess.Write);
                    StreamWriter writer = new StreamWriter (fs1);
                    writer.WriteLine ("Message =>" + Message);
                    writer.WriteLine ("Event =>" + Event);
                    writer.WriteLine ("DateTime =>" + DateTime.UtcNow.AddHours (5.5).ToString ("dd MMM yyyy hh:mm tt"));
                    writer.WriteLine ("=======================================================================================");
                    writer.Close ( );
                } else {
                    FileStream fs1 = new FileStream (fileName,
                        FileMode.Append, FileAccess.Write);
                    StreamWriter writer = new StreamWriter (fs1);
                    writer.WriteLine ("Message =>" + Message);
                    writer.WriteLine ("Event =>" + Event);
                    writer.WriteLine ("DateTime =>" + DateTime.UtcNow.AddHours (5.5).ToString ("dd MMM yyyy hh:mm tt"));
                    writer.WriteLine ("=======================================================================================");
                    writer.Close ( );
                }

            } catch (Exception ex) {
                PrintLog (ex.Message.ToString ( ), "Print Log");
            }
        }
    }
}
