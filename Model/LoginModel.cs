using System;
using System.Data;
using System.Data.Common;

using Microsoft.Extensions.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Newtonsoft.Json.Linq;

namespace TORNADO_API.Models {
    public class LoginModel {
        public IConfiguration _iconfiguration;
        public LoginModel (IConfiguration iconfiguration) {
            _iconfiguration = iconfiguration;
        }
        DatabaseProviderFactory factory = new DatabaseProviderFactory ( );
        public string userName { get; set; }
        public string Password { get; set; }
        public string IpAddress { get; set; }
        public DateTime LoginTime { get; set; }
        public DateTime LogoutTime { get; set; }
        public string LoginSucceeded { get; set; }
        public string authType { get; set; }
        public string AuthSuccess { get; set; }
        public int UserType { get; set; }
        public DataSet Select_LoginDetails()
        {
            string Connection = _iconfiguration.GetValue<string>("ConnectionStrings:LiveConnection");

            Database database;
            database = new SqlDatabase(Connection);
            DbCommand command = database.GetStoredProcCommand("[dbo].[UserAuthentication_AD]");
            DataSet ds = new DataSet();

            using (command)
            {
               
                    database.AddInParameter(command, "@UserLoginID", System.Data.DbType.String, userName);
                    database.AddInParameter(command, "@LoginPassword", System.Data.DbType.String, Password);
                    database.AddInParameter(command, "@authType", System.Data.DbType.String, authType);       //Auth Type can be AD or DB
                    // database.AddInParameter(command, "@AuthSuccess", System.Data.DbType.String, AuthSuccess); //Added to Know AD login is Successful or Not

                    ds = database.ExecuteDataSet(command);
                    return ds;
            }
        }

   

        public DataSet GetEncToken(string userName, string Password)
        {
            string Connection = _iconfiguration.GetValue<string>("ConnectionStrings:LiveConnection");

            Database database;
            database = new SqlDatabase(Connection);
            DbCommand command = database.GetStoredProcCommand("[GetTokenForADF]");
            DataSet ds = new DataSet();
            using (command)
            {
               
                    database.AddInParameter(command, "@UserLoginID", System.Data.DbType.String, userName);
                    database.AddInParameter(command, "@LoginPassword", System.Data.DbType.String, Password);
                    ds = database.ExecuteDataSet(command);
                    return ds;
               
            }
        }

        public object UpdateRefreshToken(string token, string refreshToken, DateTime expireTime, string userID, string action)
        {
            string Connection = _iconfiguration.GetValue<string>("ConnectionStrings:LiveConnection");
            Database database;
            database = new SqlDatabase(Connection);
            DbCommand command = database.GetStoredProcCommand("UserTokenUpdate");

            DataSet ds = new DataSet();
         
                using (command)
                {

                    database.AddInParameter(command, "@token", System.Data.DbType.String, token);
                    database.AddInParameter(command, "@UserLoginId", System.Data.DbType.String, userID);
                    database.AddInParameter(command, "@TokenTime", System.Data.DbType.DateTime, expireTime);
                    database.AddInParameter(command, "@RefreshToken", System.Data.DbType.String, refreshToken);
                    database.AddInParameter(command, "@action", System.Data.DbType.String, action);


                    database.AddOutParameter(command, "@Result", System.Data.DbType.Int32, -1);
                    database.ExecuteNonQuery(command);
                }
          

            JObject DBReturnResult = new JObject();

            DBReturnResult.Add("Result", (command.Parameters)[command.Parameters.Count - 1].Value.ToString());

            return DBReturnResult;

        }
        public int UserLogout(string userID)
        {
            string Connection = _iconfiguration.GetValue<string>("ConnectionStrings:LiveConnection");
            Database database;
            database = new SqlDatabase(Connection);
            DbCommand command = database.GetStoredProcCommand("UserTokenUpdate");

            DataSet ds = new DataSet();
           
                using (command)
                {

                    database.AddInParameter(command, "@token", System.Data.DbType.String, "");
                    database.AddInParameter(command, "@UserLoginId", System.Data.DbType.String, userID);
                    database.AddInParameter(command, "@TokenTime", System.Data.DbType.DateTime, null);
                    database.AddInParameter(command, "@RefreshToken", System.Data.DbType.String, "");
                    database.AddInParameter(command, "@action", System.Data.DbType.String, "logout");

                    database.AddOutParameter(command, "@Result", System.Data.DbType.Int32, -1);
                    database.ExecuteNonQuery(command);
                }
           

            // JObject DBReturnResult = new JObject();

            // DBReturnResult.Add("Result", (command.Parameters)[command.Parameters.Count - 1].Value.ToString());

            return Convert.ToInt32((command.Parameters)[command.Parameters.Count - 1].Value);

        }


        public DataSet CheckUserLogout(string userId)
        {
            string Connection = _iconfiguration.GetValue<string>("ConnectionStrings:LiveConnection");

            Database database;
            database = new SqlDatabase(Connection);
            DbCommand command = database.GetStoredProcCommand("CheckIsUserLogout");
            DataSet ds = new DataSet();
            using (command)
            {
               
                    database.AddInParameter(command, "@UserLoginId", System.Data.DbType.String, userId);
                    ds = database.ExecuteDataSet(command);
                    return ds;
            }
        }


    }
}
