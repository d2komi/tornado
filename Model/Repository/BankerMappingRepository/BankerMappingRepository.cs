using System.Data;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace TORNADO_API.Models.Repository.BankerMappingRepository {
    public class BankerMappingRepository: IBankerMappingRepository
    {
        public IConfiguration _iconfiguration;
        
        BankerMappingModel _bankerMappingModel;

        public BankerMappingRepository(IConfiguration iconfiguration) { 
            _iconfiguration = iconfiguration;
            _bankerMappingModel = new BankerMappingModel(_iconfiguration);
        }
     
        public DataSet ImportMappingFromSourceList(string Flag,string ClientName,string ProjName,string TableName)
        {
            return _bankerMappingModel.ImportMappingFromSourceList(Flag,ClientName,ProjName,TableName).GetTableName();
        }

    }
}
