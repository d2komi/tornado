using System.Data;

namespace TORNADO_API.Models.Repository.BankerMappingRepository {
  public interface IBankerMappingRepository {
        DataSet ImportMappingFromSourceList(string Flag,string ClientName,string ProjName,string TableName);
    }
}
