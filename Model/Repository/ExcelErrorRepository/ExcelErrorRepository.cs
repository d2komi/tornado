using System.Data;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace TORNADO_API.Models.Repository.ExcelErrorRepository {
    public class ExcelErrorRepository: IExcelErrorRepository
    {
        public IConfiguration _iconfiguration;
        
        ExcelErrorModel excelErrorModel;

        public ExcelErrorRepository(IConfiguration iconfiguration) { 
            _iconfiguration = iconfiguration;
            excelErrorModel = new ExcelErrorModel(_iconfiguration);
        }



        public DataSet GetErorrFileList()
        {
            return excelErrorModel.GetErorrFileList().GetTableName();
        }
        public DataSet GetErorrList(string tableName)
        {
            return excelErrorModel.GetErorrList(tableName).GetTableName();
        }

    }
}
