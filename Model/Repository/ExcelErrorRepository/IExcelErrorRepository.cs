using System.Data;

namespace TORNADO_API.Models.Repository.ExcelErrorRepository {
  public interface IExcelErrorRepository {

        DataSet GetErorrFileList();

        DataSet GetErorrList(string tableName);
    }
}
