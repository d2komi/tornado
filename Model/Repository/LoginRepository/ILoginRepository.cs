using System;
using System.Collections.Generic;
using System.Data;

namespace TORNADO_API.Models.Repository.LoginRepository {
    public interface ILoginRepository {
        DataSet SelectLoginDetails (string paramString);

        DataSet getEncToken(string userID, string password);

        object UpdateRefreshToken(string token, string refreshToken, DateTime expireTime, string userID, string action);

        int logoutUser(string userID);
        DataSet CheckUserLogout(string userId);
    }

}
