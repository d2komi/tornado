using System;
using System.Collections.Generic;
using System.Data;

using Microsoft.Extensions.Configuration;

using Newtonsoft.Json.Linq;

namespace TORNADO_API.Models.Repository.LoginRepository {
    public class LoginRepository : ILoginRepository {
        public IConfiguration _iconfiguration;
        LoginModel loginModel;
        public LoginRepository (IConfiguration iconfiguration) {
            _iconfiguration = iconfiguration;
        }

        public DataSet CheckUserLogout(string userId)
        {
            loginModel = new LoginModel(_iconfiguration);
            return loginModel.CheckUserLogout(userId).GetTableName();
        }

        public DataSet getEncToken(string userID, string password)
        {
            return loginModel.GetEncToken(userID, password).GetTableName();
        }

        public int logoutUser(string userID)
        {
            loginModel = new LoginModel(_iconfiguration);
            return loginModel.UserLogout(userID);
        }

        public DataSet SelectLoginDetails (string paramString) {
            loginModel = new LoginModel (_iconfiguration);
            JObject JData = JObject.Parse (paramString);
            loginModel.userName = JData ["userName"].ToString ( );
            loginModel.Password = JData ["Password"].ToString ( );
            loginModel.authType = JData ["authType"].ToString ( ); //Auth Type can be AD or DB
            loginModel.AuthSuccess = JData ["AuthSuccess"].ToString ( ); //Added to Know AD login is Successful or Not
            loginModel.UserType = (int) JData ["UserType"];
            return loginModel.Select_LoginDetails ( ).GetTableName ( );
        }

        public object UpdateRefreshToken(string token, string refreshToken, DateTime expireTime, string userID, string action)
        {
            loginModel = new LoginModel(_iconfiguration);
            return loginModel.UpdateRefreshToken(token, refreshToken, expireTime, userID, action);
        }
    }
}

