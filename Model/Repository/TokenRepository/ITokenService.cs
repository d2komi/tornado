﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TORNADO_API.Utility;

namespace TORNADO_API.Model.Repository.TokenRepository
{
    public interface ITokenService
    {
        string GenerateAccessToken(User user, string Secretkey, int expireationTime);
        string GenerateRefreshToken();
        ClaimsPrincipal GetPrincipalFromExpiredToken(string token, string SecretKey);
    }
}
