﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TORNADO_API.Model
{
    public class TokenApiModel
    {
        public string AccessToken { get; set; }

        public string RefreshToken { get; set; }
    }
    public class SSORedirect
    {
        public string userid { get; set; }

        public string RefreshToken { get; set; }
    }
}
