using System;
using System.Data;
using System.Data.Common;

using Microsoft.Extensions.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace TORNADO_API.Models {
    public class UserDataModel {
        public IConfiguration _iconfiguration;
        public UserDataModel (IConfiguration iconfiguration) {
            _iconfiguration = iconfiguration;
        }
        public DataSet SelectSupervisorOfficerList ( ) {
            string Connection = _iconfiguration.GetValue<string> ("ConnectionStrings:SME");

            Database database;
            database = new SqlDatabase (Connection);
            DbCommand command = database.GetStoredProcCommand ("SelectOfficerList");
            DataSet ds = new DataSet ( );
            using (command) {
                try {
                    ds = database.ExecuteDataSet (command);
                    return ds;
                } catch (Exception ex) {
                    throw ex;
                }
            }
        }
    }
}
