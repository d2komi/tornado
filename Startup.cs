﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Swagger;
using TORNADO_API.Model.Repository.TokenRepository;
using TORNADO_API.Models.Repository.ExcelErrorRepository;
using TORNADO_API.Models.Repository.BankerMappingRepository;
using TORNADO_API.Models.Repository.LoginRepository;
using TORNADO_API.Utility;

// using Swashbuckle.AspNetCore.Swagger;

namespace TORNADO_API {
    public class Startup {

     
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration) => Configuration = configuration;

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices (IServiceCollection services) {
            services.AddSignalR ();
            services.AddCors (options => options.AddPolicy ("AllowAny", x => {
                x.AllowAnyHeader ()
                    .AllowAnyMethod ()
                    .AllowAnyOrigin ();
            }));

            services.AddControllers().AddNewtonsoftJson (options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            );
            // configure strongly typed settings object
            services.Configure<AppSettings> (Configuration.GetSection ("AppSettings"));
            services.AddScoped<IExcelErrorRepository, ExcelErrorRepository> ();
            services.AddScoped<IBankerMappingRepository, BankerMappingRepository> ();


            services.AddMvc ();
            services.AddSingleton<IConfiguration> (Configuration);
            services.AddTransient<IHttpContextAccessor, HttpContextAccessor> ();
            services.AddTransient<ITokenService, TokenService> ();
            services.AddScoped<ILoginRepository, LoginRepository> ();
            

            //services.AddSwaggerGen(c =>
            //{
            //    c.SwaggerDoc("v1", new Info { Title = "SLBC_API", Version = "0.1" });
            //});
            var contact = new OpenApiContact () {
                Name = "D2k Technologies",
                Email = "user@example.com",
                Url = new Uri ("http://www.example.com")
            };

            var license = new OpenApiLicense () {
                Name = "D2k Tech",
                Url = new Uri ("http://www.example.com")
            };
            var info = new OpenApiInfo () {
                Version = "v1",
                Title = "Swagger TORNADO API",
                Description = "Swagger TORNADO API Description",
                TermsOfService = new Uri ("http://www.example.com"),
                Contact = contact,
                License = license
            };

            services.AddSwaggerGen (c => {
                c.SwaggerDoc ("v1", info);
            });

            services.Configure<FormOptions> (options => {

                options.ValueLengthLimit = int.MaxValue;
                options.MultipartBodyLengthLimit = int.MaxValue;
                options.MultipartHeadersLengthLimit = int.MaxValue;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure (IApplicationBuilder app, IWebHostEnvironment env) {

            app.UseRouting ();

            // DefaultFilesOptions DefaultFile = new DefaultFilesOptions();
            // DefaultFile.DefaultFileNames.Clear();
            // DefaultFile.DefaultFileNames.Add("helpPage.html");

            // app.UseCors(x => x.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin().AllowCredentials());
            app.UseCors ("AllowAny");
            // app.UseDefaultFiles(DefaultFile);

            #region Handle Exception
            app.UseExceptionHandler (appBuilder => {
                appBuilder.Use (async (context, next) => {
                    var error = context.Features[typeof (IExceptionHandlerFeature)] as IExceptionHandlerFeature;

                    // //when authorization has failed, should retrun a json message to client
                    // if (error != null && error.Error is SecurityTokenExpiredException)
                    // {
                    //     context.Response.StatusCode = 401;
                    //     context.Response.ContentType = "application/json";

                    //     await context.Response.WriteAsync(JsonConvert.SerializeObject(new RequestResult
                    //     {
                    //         State = RequestState.NotAuth,
                    //         Msg = "token expired"
                    //     }));
                    // }
                    // //when orther error, retrun a error message json to client
                    // else 
                    if (error != null && error.Error != null) {
                        context.Response.StatusCode = 500;
                        context.Response.ContentType = "application/json";
                        await context.Response.WriteAsync (JsonConvert.SerializeObject (new RequestResult {
                            State = RequestState.Failed,
                                Msg = error.Error.Message
                        }));
                    }
                    //when no error, do next.
                    else await next ();
                });
            });
            #endregion

            app.UseAuthentication ();
            app.UseStaticFiles ();

            //app.UseStaticFiles(new StaticFileOptions
            //{

            //    OnPrepareResponse = x =>
            //    {
            //        x.Context.Response.Headers.Add("Cache-Control", "no-store");

            //        var token = x.Context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

            //        if (token != null)
            //        {
            //            try
            //            {
            //                string Secret = Configuration.GetSection("AppSettings").GetSection("Secret").Value;
            //                if (!ValidateJwtToken(x.Context, token, Secret))
            //                {
            //                    x.Context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            //                    x.Context.Response.ContentLength = 0;
            //                    x.Context.Response.Body = Stream.Null;
            //                }

            //            }
            //            catch (Exception ex)
            //            {
            //                x.Context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            //                x.Context.Response.ContentLength = 0;
            //                x.Context.Response.Body = Stream.Null;

            //                    // do nothing if jwt validation fails
            //                    // user is not attached to context so request won't have access to secure routes
            //                }
            //        }
            //        else
            //        {
            //            x.Context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            //            x.Context.Response.ContentLength = 0;
            //            x.Context.Response.Body = Stream.Null;
            //        }
            //    }
            //});

            app.UseSwagger ();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            // app.UseSwaggerUI(c =>
            // {
            //     c.SwaggerEndpoint("/swagger/v1/swagger.json", "SLBC_API");
            // });
            //     app.UseSignalR(routes =>
            //    {
            //        routes.MapHub<MessageHub>("/message");
            //    });

            app.UseSwaggerUI (c => {
                c.SwaggerEndpoint ("/swagger/v1/swagger.json",
                    "Swagger TORNADO API v1");
            });

            //app.UseSwaggerUI(c => {
            //    if (env.IsDevelopment())
            //        c.SwaggerEndpoint("/swagger/v1/swagger.json", "CrisMAc Core API");
            //    else
            //        c.SwaggerEndpoint("/TORNADO_API/swagger/v1/swagger.json", "Swagger TORNADO API v1");
            //});

            // custom jwt auth middleware
            app.UseMiddleware<JwtMiddleware> ();

            app.UseEndpoints (x => x.MapControllers ());

        }

        private bool ValidateJwtToken (HttpContext context, string token, string Secret) {

            var tokenHandler = new JwtSecurityTokenHandler ();
            var key = Encoding.ASCII.GetBytes (Secret);
            tokenHandler.ValidateToken (token, new TokenValidationParameters {
                ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey (key),
                    ValidateIssuer = false,
                    ValidateAudience = false,

                    // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                    ClockSkew = TimeSpan.Zero
            }, out SecurityToken validatedToken);

            var jwtToken = (JwtSecurityToken) validatedToken;
            var userId = jwtToken.Claims.First (x => x.Type == "id").Value;
            if (userId != null) {
                return true;
            }
            return false;

        }

    }

}