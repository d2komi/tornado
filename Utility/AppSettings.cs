﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TORNADO_API.Utility
{
    public class AppSettings
    {
        public string Secret { get; set; }
        public string TokenTimeinMin { get; set; }
        public string RefreshTokenTimeInMin { get; set; }
    }
}
