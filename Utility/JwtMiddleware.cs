﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TORNADO_API.Models.Repository.LoginRepository;

namespace TORNADO_API.Utility
{
    public class JwtMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly AppSettings _appSettings;
        public JwtMiddleware(RequestDelegate next, IOptions<AppSettings> appSettings)
        {
            _next = next;
            _appSettings = appSettings.Value;
        }

        public async Task Invoke(HttpContext context, ILoginRepository loginService)
        {
            var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

            if (token != null)
                attachUserToContext(context, token, loginService);

            await _next(context);
        }

        private void attachUserToContext(HttpContext context, string token, ILoginRepository loginService)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,


                    // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);

                var jwtToken = (JwtSecurityToken)validatedToken;
                var userId = jwtToken.Claims.First(x => x.Type == "id").Value;


                // attach user to context on successful jwt validation
                var responce = loginService.CheckUserLogout(userId);
                var refreshTokenDB = responce.Tables["checkLogout"].Rows[0][0].ToString();
                var expireTime = responce.Tables["checkLogout"].Rows[0][1].ToString();
                var tokenDB = responce.Tables["checkLogout"].Rows[0][2].ToString();
                if (refreshTokenDB != "" && expireTime != "")
                {
                    if (tokenDB == token)
                    {
                        context.Items["User"] = userId;
                    }
                    else
                    {
                        context.Items["User"] = null;

                    }
                }
                else
                {
                    context.Items["User"] = null;
                }

            }
            catch (Exception ex)
            {
                context.Items["User"] = null;
                // do nothing if jwt validation fails
                // user is not attached to context so request won't have access to secure routes
            }
        }
    }
}