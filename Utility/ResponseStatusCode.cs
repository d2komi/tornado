#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member

namespace TORNADO_API.Utility {
    public class ResponseStatusCode {
        private ResponseStatusCode ( ) {

        }

        private const int failed = 600;
        private const int headerMissing = 601;
        private const int success = 200;

        /// <summary>
        /// Get Status code for Header Values are missing
        /// </summary>
        public static int HeaderMissing => headerMissing;
        /// <summary>
        /// Get Status Code for Failed, when error/excepetion occured
        /// </summary>
        public static int Failed => failed;
        /// <summary>
        /// Get Status Code for Success
        /// </summary>
        public static int Success => success;
    }
}
